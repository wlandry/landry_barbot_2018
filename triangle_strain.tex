%% LyX 2.3.2 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[unicode=true,pdfusetitle,
 bookmarks=true,bookmarksnumbered=false,bookmarksopen=false,
 breaklinks=false,pdfborder={0 0 1},backref=section,colorlinks=false]
 {hyperref}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
%% Because html converters don't know tabularnewline
\providecommand{\tabularnewline}{\\}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\newenvironment{lyxcode}
	{\par\begin{list}{}{
		\setlength{\rightmargin}{\leftmargin}
		\setlength{\listparindent}{0pt}% needed for AMS classes
		\raggedright
		\setlength{\itemsep}{0pt}
		\setlength{\parsep}{0pt}
		\normalfont\ttfamily}%
	 \item[]}
	{\end{list}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{authblk}
\usepackage{babel}
\usepackage{lineno}

\makeatother

\begin{document}
\title{Fast, accurate solutions for 3D strain volumes in a heterogeneous half space}

\author[1]{Walter Landry\thanks{Corresponding author.
\textit{Email Address:} wlandry@caltech.edu (W. Landry).}\thanks{Present Address: Walter Burke Institute for Theoretical Physics, Caltech, Pasadena, CA 91125, USA}}

\author[2]{Sylvain Barbot\thanks{sbarbot@usc.edu}\thanks{W. Landry: Methodology, Software, Visualization, and Writing of the paper. S. Barbot:  Methodology, Software, and Writing of this paper.}}

\affil[1]{IPAC, Caltech, Pasadena, CA 91125, USA}

\affil[2]{University of Southern California, 3651 Trousdale Parkway, Los Angeles, CA 90089, USA}

\maketitle
\begin{abstract}
Deformation in the Earth displays many degrees of localization. The
mechanics of faulting can be well represented by slip on a 2D surface
discretized in piecewise linear boundary elements. Distributed anelastic
deformation associated with fluid flow, magma transfer, or viscoelastic
relaxation can be approximated by anelastic strain discretized in
3D volume elements. The stress, traction, and displacement kernels
for these elements form the basis of forward and inverse modeling
of Earth's deformation during the seismic cycle, volcanic unrest or
hydrologic change. While there are a number of techniques for computing
these kernels for 2D fault surfaces, the techniques for 3D strain
volumes are less developed. To improve the models of Earth's deformation,
we extend previous work to numerically calculate these kernels for
3D strain volumes in a heterogeneous half space. The model provides
high-precision displacement and stress for all these cases in a self-consistent
manner. We exploit the adaptive multi-grid elastic solver implemented
in the software Gamra \cite{landry+barbot16} to compute the deformation
induced by boundary and volume elements with high numerical efficency.
We demonstrate the correctness of the method with analytic tests.
We illustrate the performance by computing a large-scale model of
postseismic deformation for the 2015 Mw 7.8 Gorkha, Nepal earthquake
with heterogeneous material properties. The open-source, freely available
software can be useful for the calculation of elasto-static Green's
functions for localized and distributed deformation in a heterogeneous
Earth.
\end{abstract}

\section{Introduction\label{sec:Introduction}}

Deformation of the Earth's lithosphere at time scales relevant to
the earthquake cycle is accommodated both by slip along 2D fault surfaces
as well as distributed strain in 3D volumes. For example, in the lower
crust and mantle asthenosphere, distributed anelastic deformation
is responsible for loading faults and accommodating transient strains
\cite{masuti+16}. The kinematics of crustal deformation can be inferred
from geodetic or seismic data during the interseismic, coseismic,
and postseismic phases of the earthquake cycle \cite{mcguire+03,murray+segall05,bartlow+11,barbot+13,sathiakumar+17,amey+18,nocquet18}.
The development of these techniques in the last few decades has led
to an explosion of knowledge on fault behavior \cite{rogers+03,bakun+05,bletery+14,wallace+17,araki+17}.

These methods have recently been extended to incorporate the distributed
deformation of large domains of the lithosphere \cite{tsang+16,lambert+barbot16,barbot+17,moore+17,qqiu+18a,barbot18a},
such that it is now possible to build models of Earth's deformation
that represent fault slip and distributed strain consistently using
elasto-static Green's functions. Analytic solutions for rectangular
and triangular dislocations \cite{chinnery61,chinnery63,savage+hastie66,sato+matsuura74,okada85,okada92,nikkhoo+15,gimbutas+12,meade07a,jeyakumaran1992modeling}
and tetrahedral and cuboidal strain volumes \cite{tsang+16,lambert+barbot16,barbot+17,barbot18a,barbot18b}
allow us to build kinematic and dynamic models of lithosphere deformation
on curved fault surfaces with distributed, off-fault anelastic strain.

Unfortunately, all these solutions share an important caveat as they
can not represent lateral variations of elastic properties. These
lateral variations can be significant, such as in the case of deep
sedimentary basins and their role in fault loading in the Los Angeles
area \cite{rollins2018interseismic}.

The goal of this paper is to describe a reliable numerical method
for constructing displacement and stress Green's functions for strain
volumes in a heteregeneous half-space. The method builds upon a three-dimensional,
adaptive, multi-grid elasticity solver that allows numerically efficient
calculation of displacement and stress kernels \cite{landry+barbot16}.
We describe the modeling approach and high precision tests of the
implementation in Section \ref{sec:Methods}. We demonstrate the relevance
of the technique by modeling postseismic deformation from the 2015
Mw 7.8 Gorkha, Nepal earthquake in Section \ref{sec:Gorkha}. The
algorithms described in this paper are implemented in Gamra, a freely
available tool for realistic earthquake modeling (see Section \ref{sec:Computer-Code-Availability}).

\section{Methods\label{sec:Methods}}

\subsection{Deformation on a Staggered Mesh}

The deformation of Earth's rocks can be broadly categorized into elastic
and anelastic deformation. In our treatment, we specify a strain (caused
by anelastic deformation) and solve for the resulting elastic deformation
in the half space \cite{andrews78,barbot+fialko10b,noda+matsuura10,barbot18a}.
The elastic problem is linear, so we can separately compute the elastic
deformation caused by individual elements of strain. Then we can add
up all of the separate solutions to find the total elastic deformation.
This linearity also enables inverse models of deformation, where at-depth
deformation is deduced from measured surface deformation. After computing
the surface deformation caused by each of the individual elements
of at-depth deformation, linearity turns the inversion problem into
a tractable matrix minimization problem \cite{mcguire+03,murray+segall05,bartlow+11,barbot+13,sathiakumar+17,amey+18,nocquet18}.

In a previous paper \cite{landry+barbot16}, those input strain elements
were only allowed to be jumps in displacement on 2D rectangular surfaces.
That enabled efficient numerical calculations of displacement and
stress kernels, resulting in fairly realistic models of earthquakes
as patches of slip along an earthquake fault. This paper generalizes
that treatment to gradients across 3D tetrahedral and cuboidal volumes.
More complicated geometries can be obtained by linear superposition.

To solve this elastic problem, we start with the equation for linear
elasticity
\begin{equation}
\sigma_{ji,j}+f_{i}=0\,,\label{eq:elasto-statics}
\end{equation}
where $\sigma_{ij}$ is the elastic part of the Cauchy stress and
$f_{i}$ is a forcing term. We use Einstein summation notation, where
each index $i$, $j$, $k$ is understood to stand for $x$, $y$,
and $z$ in turn, repeated indices are summed, and commas (,) denote
derivatives. This assumption  is thought to be valid for seismo-tectonic
activity with infinitesimal strain \cite{fialko+01c}. The elastic
properties of material in the Earth are well described by Lame's first
parameter $\lambda$, and the shear modulus $\mu$ \cite{shaw2015unified,wang2016three,simmons2010gypsum,bassin2000current,dziewonski+anderson81}.
This allows us to write the elastic stress in terms of the elastic
displacement $v_{i}$ as
\begin{equation}
\sigma_{ji}\left(\vec{v}\right)\equiv\mu(v_{i,j}+v_{j,i})+\delta_{ij}\lambda v_{k,k\,}.\label{eq:Stress}
\end{equation}

Input strains are introduced through the forcing terms $f_{i}$. These
forcing terms are carefully constructed to modify the solution for
the derivative of the displacement $v_{i,j}$ so that they match the
input strain. The displacement across a fault can be a step function.
Although the strain away from the fault is well behaved, the strain
on the fault itself would be a delta function. This requires us to
be especially careful to avoid numerical difficulties. 

Since we are also interested in quasi-static deformation, we can follow
a similar line of reasoning to look at the time derivative of stress
$\partial\sigma_{ij}/\partial t$ in terms of the velocity of deformation
$\partial v_{i}/\partial t$. This results in formulas with the same
functional form as equations \ref{eq:elasto-statics} and \ref{eq:Stress}.
Because a large breadth of physical processes can be captured within
the same functional form, solving equations \ref{eq:elasto-statics}
and \ref{eq:Stress} with realistic material properties can find a
broad range of applications \cite{tsang+16,moore+17,qqiu+18a,barbot18a}.

The numerical method we use to solve these equations is largely described
in a previous paper \cite{landry+barbot16}. To summarize, we use
a parallel multigrid solver on a staggered, adapted finite difference
grid as in Figure \ref{fig:Fault-corrections}. To account for the
input strain, we add carefully constructed forcing terms $\delta f_{i}$
that depend on the mesh size. For example, when Eq. \ref{eq:elasto-statics}
is expanded out, it includes the term $\left(\mu v_{x,x}\right)_{,x}$.
Expressing this derivative at point $A=\left(A_{x},A_{y}\right)$
in Figure \ref{fig:Fault-corrections} using standard finite differences
gives

\begin{multline}
\left.\left(\mu v_{x,x}\right)_{,x}\right|_{A_{x},A_{y}}=\left[\left.\mu\right|_{A_{x}+\delta x/2,A_{y}}\left(\left.v_{x}\right|_{A_{x}+\delta x,A_{y}}-\left.v_{x}\right|_{A_{x},A_{y}}\right)\right.\\
\left.-\left.\mu\right|_{A_{x}-\delta x/2,A_{y}}\left(\left.v_{x}\right|_{A_{x},A_{y}}-\left.v_{x}\right|_{A_{x}-\delta x,A_{y}}\right)\right]/\delta x^{2},\label{eq:finite_difference}
\end{multline}
where $\delta x$ is the width of each finite difference cell and
the notation $\left.\mu\right|_{x,y}$ denotes the value of $\mu$
at the coordinate $\left(x,y\right)$. For the fault near $A$, the
fault creates a step function $s_{i}$ in the solution for the displacement
$v_{i}$. In this case, the appropriate forcing term at $A$ for the
$\left(\mu v_{x,x}\right)_{,x}$ term turns out to be

\begin{equation}
\left.\delta f_{x}\right|_{A_{x},A_{y}}=-s_{x}\left.\mu\right|_{A_{x}+\delta x/2,A_{y}}/\delta x^{2}.\label{eq:forcing_correction}
\end{equation}

Since the displacement $v_{i}$ is no longer continuous, we also have
to take care when interpolating results for multigrid. After working
through all of the details, described in \cite{landry+barbot16},
the result is that the interpolation stencils acquire some extra constant
terms.

\begin{figure}
\begin{centering}
\includegraphics[width=0.3\paperwidth]{strain_volumes} 
\par\end{centering}
\caption{\label{fig:Fault-corrections}Dislocations and strain volumes on a
staggered grid. In 2D, dislocations are lines and strain volumes are
triangles or rectangles. In 3D, the dislocations are triangles or
rectangles, and strain volumes are either tetrahedra or cuboids. So
this figure can represent either 2D grids or slices of 3D grids. The
stencil for $\left(\mu v_{x,x}\right)_{,x}$ at point $A$ gives rise
to a forcing term proportional to the slip $s_{x}$. At point $B$,
the forcing term is proportional to $\epsilon_{xx}\delta x_{\text{traversed}}$.}
\end{figure}


\subsection{3D Strain Volumes}

The main advance of this paper is the generalization of this framework
to strain volumes. We consider tetrahedra or cuboids with a piecewise
uniform anelastic strain $\epsilon_{ij}$. A realistic distributed
strain can be modeled by adding up many of these individual elements.
This uniform strain corresponds to a gradient in the displacement.
Consider Figure \ref{fig:Fault-corrections}. The computed difference
in $v_{i}$ between $\left(B_{x}+\delta x,B_{y}\right)$ and $\left(B_{x},B_{y}\right)$
depends on how much of the strain volume is traversed by the stencil.
In this case, the difference for $v_{x}$ is $\epsilon_{xx}\delta x_{\text{traversed}}$.
Using this, we compute a forcing term for strain volumes in a manner
similar to that used to compute Eq. \ref{eq:forcing_correction},
giving
\begin{equation}
\left.\delta f_{x}\right|_{B_{x},B_{y}}=-\epsilon_{xx}\delta x_{\text{traversed}}\left.\mu\right|_{B_{x}+\frac{\delta x}{2},B_{y}}/\delta x^{2}.\label{eq:strain_slip_correction}
\end{equation}
Comparing this with Eq. \ref{eq:forcing_correction}, the only difference
is replacing $s_{x}$ with $\epsilon_{xx}\delta x_{\text{traversed}}$.
This means that after computing $\epsilon_{ij}\delta x_{\text{traversed}}^{j}$
for all of the strain volumes, we can reuse all of the machinery for
adaptive multigrid solutions that we previously employed for dislocations.

For testing our implementation, we start with the analytic solution
for a cuboid aligned with the surface \cite{lambert+barbot16,barbot+17}.
Figure \ref{fig:strain-J2} shows the cuboid, constructed out of 6
tetrahedra, and the computed solution for this arrangement. Table
\ref{tab:vx_error_strain} demonstrates the convergence of the $\ell_{\infty}$
error in $v_{x}$. We also implement cuboids directly, and the solutions
are identical to the solutions of cuboids made up of tetrahedra.

\begin{figure}
\begin{centering}
\includegraphics[width=0.4\paperwidth]{strain_with_volume_labeled}
\par\end{centering}
\caption{\label{fig:strain-J2}A cutout of the invariant of the scaled deviatoric
stress $J_{2}=\left(\sigma_{ij}\sigma_{ji}-\sigma_{ii}\sigma_{jj}/3\right)/2$
of a computed solution for a single cuboidal strain volume. The equivalent
resolution of the finest level is $256\times256\times256$. The strain
volume, indicated in grey, is made up of 6 tetrahedra and has dimensions
$L=0.50$, $W=0.25$, $H=0.08$. Each tetrahedra has shear strain
components $\epsilon_{LH}=\epsilon_{HL}=1600$, so the total slip
across the narrow part of the block is $s=\epsilon_{LH}\times H=1600\times0.08=128$.
The moduli are constant ($\mu=1.6$, $\lambda=1.5$). We set the boundary
conditions (normal Dirichlet and shear stress) from the analytic solution
in \cite{lambert+barbot16,barbot+17}.}
\end{figure}

\begin{table}[h]
\begin{centering}
\begin{tabular}{|l|l|}
\hline 
$h$ & $\ell_{\infty}\left(\delta v_{x}\right)$\tabularnewline
\hline 
\hline 
0.1 & 26.54\tabularnewline
\hline 
0.05 & 3.191\tabularnewline
\hline 
0.025 & 1.545\tabularnewline
\hline 
0.0125 & 1.222\tabularnewline
\hline 
0.00625 & 0.4033\tabularnewline
\hline 
\end{tabular}
\par\end{centering}
\caption{\label{tab:vx_error_strain}The $\ell_{\infty}$ norm of the error
of $v_{x}$ for a computed solution of a cuboid strain volume as a
function of grid spacing $h$, where $\ell_{\infty}\left(\delta v_{x}\right)\equiv\max\left|\left.v_{x}\right|_{computed}-\left.v_{x}\right|_{analytic}\right|$.
The strain volume has shear strain components $\epsilon_{LH}=\epsilon_{HL}=1600$
and dimensions $L=0.50$, $W=0.25$, $H=0.08$. The moduli are constant
($\mu=\lambda=1$). We set the boundary conditions (normal Dirichlet
and shear stress) from the analytic solution in \cite{lambert+barbot16,barbot+17}.
The displacement is regular, so the convergence is uneven but monotonic.}
\end{table}

To test more complicated volumes with variable moduli, we create a
model using one of the tetrahedra from Figure \ref{fig:strain-J2}.
Figure \ref{fig:tetra_mesh} shows the dimensions of the tetrahedra,
and Figure \ref{fig:strain3D_tetra} shows a solution for the stress.
We do not have an analytic solution for this setup, but we can approximate
a tetrahedral strain volume with a number of small triangular faults
as in Figure \ref{fig:tetra_mesh}. This approach works very well
at coarse resolution. At higher resolution, the mesh can resolve the
spaces between the faults, so the details start to differ more significantly.
Table \ref{tab:vx_fault_diff} demonstrates that the two methods converge
as the number of faults increases. This gives us some confidence in
the correctness of our implementation of strain volumes with heterogeneous
elasticity.

\begin{figure}
\begin{centering}
\includegraphics[width=0.4\paperwidth]{strain3D_tetra_mesh}
\par\end{centering}
\caption{\label{fig:tetra_mesh}Dimensions of the single tetrahedra used for
testing variable moduli in strain volumes. To verify the implementation,
we also approximate the strain volume with a large number of triangular
faults distributed along the $H$ axis. The shaded triangle is a representative
single fault. So the triangular faults start out as big as the side
of the tetrahedra and then converge to a single point. }
\end{figure}

\begin{figure}
\begin{centering}
\includegraphics[width=0.4\paperwidth]{strain3D_tetra_labeled}
\par\end{centering}
\caption{\label{fig:strain3D_tetra}A cutout of the invariant of the scaled
deviatoric stress $J_{2}=\left(\sigma_{ij}\sigma_{ji}-\sigma_{ii}\sigma_{jj}/3\right)/2$
of a computed solution for a single tetrahedral strain volume. The
equivalent resolution of the finest level is $64\times64\times64$.
The tetrahedra is the same as one of the tetrahedra making up the
block in Figure \ref{fig:strain-J2}, and has a single non-zero shear
strain component $\epsilon_{LH}=1600$. The moduli are not uniform,
but set to $\mu=\mu_{0}+3x+5y$, $\lambda=\lambda_{0}-2x+7y$, where
$\mu_{0}=10$ and $\lambda_{0}=10$. The boundary conditions are zero
normal displacement and zero stress.}
\end{figure}

\begin{table}
\begin{centering}
\begin{tabular}{|l|l|}
\hline 
$N$ & $\ell_{\infty}\left(\delta v_{x}\right)$\tabularnewline
\hline 
\hline 
128 & 0.4129\tabularnewline
\hline 
256 & 0.1645\tabularnewline
\hline 
512 & 0.0750\tabularnewline
\hline 
1024 & 0.0520\tabularnewline
\hline 
\end{tabular}
\par\end{centering}
\caption{\label{tab:vx_fault_diff}The $\ell_{\infty}$ norm of the difference
in $v_{x}$ between solutions that use a tetrahedral strain volume
directly and one approximated with $N$ small triangular faults. The
setup is the same as in Figure \ref{fig:strain3D_tetra}. The triangular
faults are evenly distributed along the width ($H=0.08)$ as shown
in Figure \ref{fig:tetra_mesh}. The slip on each triangular fault
is $\epsilon_{LH}\times H/N$. }
\end{table}


\section{The 2015 Mw 7.8 Gorkha, Nepal earthquake\label{sec:Gorkha}}

As an illustration of the relevance and performance of the proposed
modeling approach, we model the initial post-seismic relaxation of
the 2015 Mw 7.8 Gorkha, Nepal earthquake to incorporate hetergeneous
material properties. This earthquake \cite{avouac+15,galetzka+15,elliott+16}
took place on the Main Himalayan Front, a megathrust that separates
the Indian and Eurasian plates \cite{tapponnier+86}. The main shock
was followed by a detectable transient deformation \cite{wang+fialko16,gualandi+16}
that was compatible with calculations of accelerated fault slip in
the down-dip extension of the rupture and viscoelastic flow in the
lower crust of Southern Tibet \cite{zhao+17}. Our goal is to partially
replicate these calculations by computing the instantaneous velocity
induced by viscoelastic flow in the lower crust of the down-going
plate. This calculation in the heterogeneous Earth has not been possible
without constructing complicated meshes.

To do this, we first use Gamra to compute the stresses caused by the
initial earthquake using the realistic slip model from \cite{qqiu+16}
(Figure \ref{fig:Gorkha_slip}). We set the elastic moduli using the
spherically symmetric 1D Preliminary Reference Earth Model (PREM)
\cite{dziewonski+anderson81}. Other than a straightforward extension
of Gamra to handle triangular patches, this is an application of the
algorithm described in a previous paper \cite{landry+barbot16}.

\begin{figure}
\begin{centering}
\includegraphics[width=0.6\paperwidth]{Gorkha_slip_induced_computed}
\par\end{centering}
\caption{\label{fig:Gorkha_slip}A cutout of the input slip and computed displacement
for the 2015 Mw 7.8 Gorkha, Nepal earthquake. The scale of the model
is marked in km, and the political boundaries of Nepal are superimposed
for reference.}
\end{figure}

This gives us a stress distribution throughout the bulk. This stress
causes viscoelastic flow in the lower crust. We model the lower crust
as a ramp, centered on the Main Himalayan Front, that dives from 20-40
km down to 50-70 km \cite{cattin+avouac00}. We discretize this ramp
using 2304 cuboids, each $15\times15\times5$ km. Dividing the computed
stress by an assumed viscosity of $10^{20}$ Pa$\cdot$s gives us
an instantaneous strain rate on each cuboid.

Using these strain rate cuboids as input, we use Gamra again, but
this time computing the instantaneous strain rate in the bulk (Figure
\ref{fig:Gorkha_strain}). The solution encompasses a box $600\times600\times300$ 
km. The boundary conditions on the side and bottom are free slip:
zero shear stress rate and zero normal velocity. The boundary conditions
on the top are free surface: zero shear velocity and zero normal stress
rate. 

The adapted mesh ranges from a resolution of 9,300 m down to 146 m.
This is equivalent to a fixed resolution of $4096\times4096\times2048$.
We set the refinement criteria to refine when the difference between
solutions in the induced velocity at different levels of refinement
error is greater than $10^{-11}$ cm/s. This is about 20 times smaller
than the error due to the boundaries being only 300 km away. A uniform
mesh would require about $3\times10^{10}$ elements, but the adapted
mesh only requires $6\times10^{7}$ elements. 

In comparison with our previous work with faults made up of 2D surfaces,
viscoelastic deformation occurs at a greater depth and is more distributed.
The resulting strain rate is more diffuse, requiring relatively more
mesh refinement over a larger volume. The viscoelastic model took
26 hours to solve on a Dell R720 with 16 physical cores (Intel Xeon
CPU ES-2670).

\begin{figure}
\begin{centering}
\includegraphics[width=0.6\paperwidth]{Gorkha_strain_induced_computed}
\par\end{centering}
\caption{\label{fig:Gorkha_strain}A cutout of the input and computed instantaneous
strain rates for the 2015 Mw 7.8 Gorkha, Nepal earthquake. We plot
the second invariant of the symmetrized strain rate $J_{2}\left(\epsilon\right)=\left(\epsilon_{ij}\epsilon_{ji}-\epsilon_{ii}\epsilon_{jj}/3\right)/2$.
The scale of the model is marked in km, and the political boundaries
of Nepal are superimposed for reference.}
\end{figure}


\section{Conclusions and future work}

We have demonstrated that we can robustly and accurately model arbitrary
distributions of strain in tetrahedral and cuboidal  volumes embedded
in a hetergeneous half space. A particular advantage of the adaptive
mesh is that computing the  velocity field due to any single strain
volume is faster than for the overall lower crust model, because the
mesh can remain coarse far away from the source. This makes the approach
well suited for the calculation of elasto-static Green's functions
for localized (e.g., faulting) and distributed (e.g., viscoelastic)
deformation.

While the approach is sufficient for a large class of problems, there
are still some significant limitations. Topography can play a major
influence on the surface displacements when there are large topographic
gradients or if the deformation source is shallow (such as during
volcanic unrest \cite{cayol98,williams+wadge00} or underground explosions
\cite{wang+18}). In addition, the effect of the Earth's curvature
can play an important role for long-wavelength deformation \cite{pollitz96}.
Also, coupling of deformation with the gravity field \cite{pollitz96}
is detectable for particularly large earthquakes \cite{han+06,han+14,vallee+17}.
These areas will be the focus of future work.

\section{Acknowledgements}

This research was supported by the National Research Foundation of
Singapore under the NRF Fellowship scheme (National Research Fellow
Award No. NRF-NRFF2013-04) and by the Earth Observatory of Singapore
and the National Research Foundation and the Singapore Ministry of
Education under the Research Centres of Excellence initiative.

\section{Computer Code Availability\label{sec:Computer-Code-Availability}}

The algorithms described in this paper are implemented in Gamra \cite{landry+barbot16},
which is available at \href{https://bitbucket.org/wlandry/gamra}{https://bitbucket.org/wlandry/gamra}.
Gamra is written in C++, uses MPI for parallelism, and depends on
a number of packages: HDF5 \cite{hdf5}, SAMRAI \cite{hornung+02,hornung+06,samrai+online},
FTensor \cite{ftensor+article,ftensor+online}, libokada \cite{libokada},
muparser \cite{muparser-home-page}, and Boost \cite{boost}. Gamra
runs on everything from laptops to supercomputers. While Gamra is
still under active development, the version associated with this paper
has the Mercurial \cite{mercurial} changeset ID
\begin{lyxcode}
b2d412042fb39cf780ccce431bfb4f476ac74bd7
\end{lyxcode}
\bibliographystyle{abbrv}
\bibliography{reference}

\end{document}
